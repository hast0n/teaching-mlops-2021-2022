from fastapi import FastAPI
import spacy
import logging
from json import dumps
from fastapi.responses import RedirectResponse
from starlette.exceptions import HTTPException as StarletteHTTPException

logging.info("Loading model..")
nlp = spacy.load("./models")

app = FastAPI()


@app.exception_handler(StarletteHTTPException)
async def custom_http_exception_handler(request, exc):
    return RedirectResponse("/docs")


@app.get("/api/intent")
async def intent_inference(sentence: str):
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return dumps(inference.cats)


@app.get("/api/intent-supported-languages")
async def supported_languages():
    return dumps(["fr-FR"])


@app.get("/health", status_code=200)
async def health_check_endpoint():
    return True


# lien de l'url en vrai: http://localhost:8080/api/intent-supported-languages

# c'est le seul fichier qu'on doit tester, pas d'envoi ou de réception
# de message depuis l'API
