FROM python:3.8.8

# RUN adduser -D admin
# USER admin

ENV PORT=$PORT
# EXPOSE $PORT

WORKDIR /usr/src/app

RUN pip install spacy==2.3.0 fastapi==0.70.1 uvicorn==0.16.0

COPY . ./

RUN echo $PORT

CMD uvicorn main:app --host=0.0.0.0 --port=$PORT