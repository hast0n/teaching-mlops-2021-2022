Réponses aux questions:

* "Vos services sont vachement gourmand coté ML. Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?"

Après avoir utilisé la commande "docker images" dans notre pipeline, la taille de notre image Docker est 1.46 Go.
Concernant la limite de RAM, nous avons visualisé cela à l'aide de locust. Avec 1800 utilisateurs (limite de notre trafic pour que le système reste viable), la RAM utilisée est de 1.52 GB, donc la limite de RAM à définir pour nos environnements est de 2048 MB. Voici une capture d'écran pour prouver nos propos:

![image.png](./image.png)

* "Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99 < 200ms"

```
users         99%ile (ms)         RPS 
2200          ~360               ~512 
2000          ~280               ~650                 
1800          ~180               ~600


```



D'après ce tableau de résultats notre modèle est viable jusqu'à 1800 utilisateurs.

* "On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, t'as des idées pour améliorer les perfs ?"

Une première idée pour améliorer les performances est de parallélser les tâches qui arrivent en entrée du modèle.
Pour cela, il est par exemple possible d'utiliser le framework de parallélisation CUDA, créé par Nvidia. L'architecture CUDA est constituée de hosts et de devices. Les hosts sont  des CPU, alors que les devices sont des GPU. CUDA permet de créer des kernels, qui sont des fonctions pour effectuer plusieurs tâches en parallèle. Quand un kernel est lancé, il va être exécuté simultanément par plusieurs threads. Ceci permet d'accélérer grandement l'entraînement et l'inférence d'un modèle.

Pour améliorer les performances, une autre possibilité intéressante est de réduire le temps d'inférence du modèle en place.
Pour cela, plusieurs méthodes existent:

. La quantization: pour un modèle, chaque poids est encodé sous une certaine forme; par exemple, un float64 qui prend 64 bits. La quantization consiste à prendre chaque poids et à le convertir en une forme qui soit moins consommatrice en mémoire; par exemple, un float16 qui prenne 16 bits. L'inconvénient est que le modèle risque d'avoir une accuracy qui diminue.

. Le pruning: Lors de l'entraînement d'un modèle, il arrive que certains neurones aient des fonctionnalités similaires, voire redondantes. Si 2 neurones n1 et n2 sont redondants, le rôle du pruning est de réduire un des neurones à 0. L'inférence est ainsi plus rapide.

. Le pooling: Très utilisé en traitement d'images, cette méthode consiste à prendre un groupe de données et à ne garder que la moyenne de ces données, par exemple. Dans le cas d'une image, l'average pooling consiste à prendre un groupe de n*n pixels, à ne garder que la moyenne de ces pixels, et de constituer une nouvelle image à partir de ces moyennes.
