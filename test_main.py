import pytest
from main import *


@pytest.mark.asyncio
async def test_supported_languages():
    assert "fr-FR" in await supported_languages()


# ce qu'on veut, c'est vérifier que pour la méthode supported_languages,
# on a bien 'fr-Fr' dans la liste des retours
